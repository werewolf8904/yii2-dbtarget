<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 12.08.2018
 * Time: 18:16
 */

namespace werewolf8904\dblog;

use Yii;

class DbTarget extends \yii\log\DbTarget
{


    /**
     * Maxtime, after witch log message will be deleted, false if none
     *
     * @var int|boolean
     */
    public $lifetime = 604800;

    /**
     * @var int the probability (parts per million) that garbage collection (GC) should be performed
     * when storing a piece of data in the cache. Defaults to 100, meaning 0.01% chance.
     * This number should be between 0 and 1000000. A value 0 meaning no GC will be performed at all.
     */
    public $gcProbability = 50000;

    public function export()
    {
        parent::export();

        try {
            $this->gc();
        } catch (\Exception $e) {
            Yii::warning("Unable to clean log: {$e->getMessage()}", __METHOD__);
        }


    }

    /**
     * Removes the expired data values.
     *
     * @param bool $force whether to enforce the garbage collection regardless of [[gcProbability]].
     *                    Defaults to false, meaning the actual deletion happens with the probability as specified by
     *                    [[gcProbability]].
     *
     * @throws \yii\db\Exception
     */
    public function gc($force = false)
    {
        if ($this->lifetime) {

            if ($force || mt_rand(0, 1000000) < $this->gcProbability) {
                $time = time() - $this->lifetime;

                $this->db->createCommand()
                    ->delete($this->db->quoteTableName($this->logTable), ['<=', 'log_time', $time])
                    ->execute();
            }

        }
    }
}